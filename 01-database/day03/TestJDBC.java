package jdbc;

// JDBC提高接口规范：java.sql.*
import java.sql.*; 	//自动把这个包下的所有类都导入

/**
	@author 作者： CGB.NUTONY 
	@author 邮箱：chenzs@tedu.cn
	@version 版本：1.0 上午11:53:50
	@desc 功能：java访问数据库
*/

public class TestJDBC {
	public static void main(String[] args) throws Exception {
		/*
		 * 开发步骤：
		 * 1、注册驱动：com.mysql.jdbc.Driver
		 * 2、DriverManage.getConnection() 获取链接，通过这个链接来访问数据库
		 * 3、创建Statement对象，进行sql查询执行
		 * 4、执行sql语句后返回 ResultSet 结果集，二维表格（行，列（字段）
		 * 5、获取表的元信息（列名），列的个数
		 * 6、获取某个行某个字段它的值，进行遍历打印最终数据库表获取的结果到控制台
		 */
		
		//Class.forName("com.mysql.jdbc.Driver");	//固定值，注册
		Class.forName("com.mysql.cj.jdbc.Driver");	//固定值，注册
		//jdbc:mysql:// 协议头，http://www.baidu.com
		//localhost实质是ip地址：127.0.0.1 代表本机
		//mysql 3306端口，端口就是区分同一台机器不同服务（mysql），端口号是唯一
		//访问数据库服务器ip地址和端口
		//参数：一会我们会有错误，通过错误配置对应解决参数
		//可以省略成///斜杠，前提localhost，端口3306
		
		//第一个参数前面使用?问号，?characterEncoding=utf8 设置字符集和数据库一致，u8
		//第二个参数如果mysql8.0以上必须用新的驱动程序，旧的会报错，必须用&符号
		String url = "jdbc:mysql://localhost:3307/py-school-db"
				+ "?characterEncoding=utf8"
				+ "&serverTimezone=Asia/Shanghai";			
		String user = "root";	//用户名
		String password = "root"; //密码
		
		//获取访问数据库链接，和数据库创建了一个通道
		//password cannot be resolved to a variable password变量必须定义
		Connection cn = DriverManager.getConnection(url, user, password);
		
		//创建语句对象，通过它来执行sql语句
		Statement stat = cn.createStatement();
		
		String sql = "select * from teachers where tname='陈冰'";		//注意里面不需要;结尾
		ResultSet rs = stat.executeQuery(sql);		//执行查询的sql语句
		System.out.println("查询SQL语句："+sql);
		
		//遍历结果集，元数据（列名）打印
		//从元数据（修饰表） MetaData元数据，getColumnCount获取列（字段）个数
		int cols = rs.getMetaData().getColumnCount();
		System.out.println("表的字段个数：" + cols);
		
		for(int i=1; i<=cols; i++) {
			//从元数据中获取列名，数据库的列是从1开始，ColumnName列名
			//tab键，转义字符，斜杠后不是字母t，而是tab键（\t）
			System.out.print( rs.getMetaData().getColumnName(i) +"\t" );
		}
		
		//遍历数据（老师信息）ResultSet结果集，它next()是否有下一条记录
		//next()方法如果有记录就返回true，继续循环，直到最后，没有记录，
		//返回false，while循环就结束了
		while( rs.next() ) {
			System.out.println();   //换行
			//获取某个字段值，获取结果集中第一列，返回字符串类型
			//System.out.println( rs.getString(2));
			
			//通过for循环获取每个列
			for(int i=1; i<=cols; i++) {
				System.out.print( rs.getString(i)+"\t" );
			}
		}
	}
}





