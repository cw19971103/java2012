# day03
## 知识回顾
### 表设计
了解软件企业中软件开发流程，它怎么去进行表的设计，或者说实际开发中表是怎么来的？
系统分析师他来进行设计，设计表和表的关系，表字段、主键、外键等等。
PowerDesigner进行设计，可以直观看到表和表直接关系，可以看表的相关内容，它还可以自动生成建表语句。

pdm 物理数据模型（表设计）会看，会preview复制sql语句即可。

在软件企业中对应初级程序员要求，直接看懂数据库表设计，能写java代码即可。
  
### 数据类型：java和sql习惯不同  
-  java区分大小写，sql不区分大小写
-  java字符串String，sql字符串varchar(6) （abc，实际长度3）变长字符串、char(6)定长字符串（abc后面三个空格）
-  java整数byte,short,int,long；sql mysql：tinyint小整型、int、long
-  java浮点数float,double；sql mysql：numeric、decimal；decimal(8,2)6位整数，2位小数
-  java日期 java.util.Date；sql mysql：datetime（年月日时分秒）、timestamp时间戳（long型，1900）
-  java复杂对象，mysql 表

### 表的关系表达：表之间可能有关系，也可能无关系
- 4种：1:1，1:n，n:1，n:n
- 体现外键上
-  1:1，它在从表中加一个外键值，和从表自身主键可以合并，主外键是一个字段（特殊情况）
-  1:n,n:1，它在从表中增加一个外键值，外键值独立存在，单独一个字段。每个员工都有一个部门编号（外键）
-  n:n，理解成两个的1:n，需要一个额外的表来存储双方的主键。scores成绩表，sno学生外键，cno课程外键。中间表

### SQL面试：必考，SQL知识是初级程序员必会知识，分布式主流架构下，对传统数据库SQL要求降低，10%要求  
只需要大家把这40道题反复做，看到题目就能写出SQL。面试基本就过关。  
特别面试前要来抱佛脚！！！

### 条件过滤
where和select语句，查询完进行数据过滤，内部先形成笛卡尔积，然后进行数据过滤  
group by和having，分组进行处理，把查询结果分成多个堆，对每个堆进行过滤  
join和on，用于多表联查，在多表结果集形成一个大表，大表（临时表，也在内存中）on过滤这个结果集  
它们有自己语法，用法不同

##今天任务：
1）JDBC java来访问mysql数据库（oracle、sqlserver），程序和数据库交互
2）GIT、GITEE，团队协作开发，共同开发，协作工具

## JDBC (java database connectivity）java连接数据库。以后开发都是使用jdbc
后期三大框架中的mybatis框架底层就封装了jdbc

JDBC是一套规范，sun公司出品，它不做具体事情，只进行规则的定义，数据库厂商自己去实现这套标准
1）规范java接口（java.sql.*）
2）mysql提供jar包，oracle提供jar包（每个数据库厂商自己独立实现）
这些jar包称为驱动包，去数据库厂商官网去找

下载jar包：两个mysql驱动
老师仓库：nutony/res/jars/jdbc/

存储过程过气技术，主流已经不许使用，阿里大厂都禁止
它每个厂家对SQL规范进行极大扩展，它有自己语法，存储过程就数据库编程（java）
oracle和mysql存储过程语法不同。a客户oracle,b客户mysql。开发不愿写2套
java支持，把存储过程使用java语言实现。
把这种情况叫做数据库迁移（换数据库），做不了，软件企业就不让用存储过程。  
它不适合分布式开发，存储在多台服务器。mysql/oracle都是集中式（放在一台服务器）

### JDBC程序开发步骤：
1）eclipse 4.7以上，创建 java工程  
2）在工程下创建lib目录和src平齐，把下载驱动jar包，拷贝进去 mysql-connector-java-5.1.32.jar  
3）让项目知道jar，build path（引入第三方jar），这样系统才可以适应  
4）创建TestJDBC.java类，创建main方法，写jdbc代码  

![输入图片说明](https://images.gitee.com/uploads/images/2021/0201/115430_23d7497d_2325268.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0201/115631_ead3a547_2325268.png "屏幕截图.png")

### 数据库链接参数 url  
1）中文乱码参数：characterEncoding=utf8  
2）8.0高版本参数：驱动要求 com.mysql.cj.jdbc.Driver，同时时区 serverTimezone=Asia/Shanghai  


### Git版本控制：
CVS,SVN,Git（Gitee）
写代码忘了保存，丢失了。文档很久没访问，找不到。文档写了很多遍，前面都被覆盖。
CVS可以保存多个文档，1.0文档，1.1文档。每次保存，形成新的版本。可以回退到旧的版本。  
类似事务检查点。回滚后检查点就消失，或者提交后，检查点就消失。而版本控制是不会消失。  
特点：很占用空间。  
CVS几乎淘汰，SVN还有在使用，管理代码和项目文档（合同），可以进行细粒度权限控制。集中式。必须要求有网络。  
主流GIT，GIT分布式，可以暂时没有网络。管理代码。可以进行粗粒度权限控制。整个仓库公开或者私有。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0201/164144_c59fd7a1_2325268.png "屏幕截图.png")

### git的概念：  
4个部分：    
工作空间：workspace，自己的代码保存在这里  
本地索引：index，也称暂存区，保存操作日志（新建文件，删除文件）  
本地仓库：Local Repository，复制工作空间代码（备份）可以脱离外网  
远程仓库：github/gitee（镜像仓库）  

### git命令
add 新增，把工作空间的代码放到本地索引  
commit 提交，把本地索引放入到本地仓库  
push 推送，把本地仓库内容放到远程仓库  
clone 克隆，第一次执行（完全），把远程仓库内容下载到工作空间  
pull 拉取，把远程仓库内容拉取到工作空间  


## 总结 
### JDBC
java访问数据库api集合。java.sql.*接口规范，数据库厂商实现类。
需要导入第三方包：mysql-connector-java-5.1.32.jar
项目导入包，src目录平齐lib，包jar考入，必须在builder path，项目引入

TestJDBC.java
### 开发步骤：  
- 1）注册 Class.forName("com.mysql.jdbc.Driver");
- 2) 获取链接：Connection cn = DriverManage.getConnection();
- 3) 创建Statement对象
- 4) 执行executeQuery（sql），执行sql语句，查询
- 5）返回ResultSet结果集对象，包括元数据（表列，字段，类型），业务数据（老师表）
- 6）变量集合 for(int i=1; i<cols; i++){ ... }

### 设置url   
jdbc:mysql://localhost:3306/py-school-db?characterEncoding=utf8&serverTimezone=Asia/Shanghai  
characterEncoding防止乱码  
serverTimezone时区，新8.0驱动，必须写   

### 连接方式可以三种：   
1）statement 语句，常用，执行SQL    
2) preparedStatement 预编译语句，提前进行编译，参数部分？占位符，可以多个    
3) callStatement 存储过程，已经废弃，项目不许使用。难维护，难调试，不支持分布式    

### statement和preparedStatement区别：  
1）prepared是预编译，sql缓存，性能高。statement每次提交sql都需要编译，性能低    
2）安全性，statement致命缺点sql注入，黑客就利用这个非法登录；prepared利用占位符方式，不会有安全问题    
在实际开发中，推荐使用prepared方式。mybaits它就默认使用perpared  

### 怎么改造statement到prepared？  
1) 改造sql：select * from teachers where tname = ?  
2）statement stat对象换成preparedStatment ps  
3）ps多设置值把?占位符替代  ps.setString(1,"陈冰");  
4）执行 ps.executeQuery();  

JDBC已经“淘汰”，开发人员不直接使用JDBC代码，主流已经使用框架把JDBC封装。  
hibernate过气，有少量企业在用，mybatis主流，这些框架底层就是JDBC代码。  


### GIT

### 创建一个gitee仓库

### 任务：  
1）创建gitee账号，安装http://act.codeboy.com/tools/00/ 22-01-Git-2.12.0-64-bit.exe  
2）安装HbuilderX，明天开始学习web前端知识，FTP下载超级文档：培优-JavaEE-v4.0-web.docx  
https://www.dcloud.io/ 下载安装，  
