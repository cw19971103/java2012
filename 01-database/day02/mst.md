

 ### 1	查询students表的所有记录
SELECT *  
FROM students;  


 ### 2	查询students表中的所有记录的sname、ssex和class列
SELECT sname,ssex,class  
FROM students;  


 ### 3	查询编号为101的同学记录
SELECT * FROM students  
WHERE sno='101';  


 ### 4	查询姓“王”的同学记录
SELECT *  
FROM students  
WHERE sname LIKE '王%';   

#查询不姓“王”的同学记录  
SELECT *  
FROM students  
WHERE sname NOT LIKE '王%';   

#查询姓名中含“王”的同学记录  
SELECT *  
FROM students  
WHERE sname LIKE '%王%';  


 ### 5	查询scores表中成绩在60到80之间的所有记录
SELECT * FROM scores  
WHERE degree BETWEEN 60 AND 80;  

SELECT * FROM scores WHERE degree>=60 AND degree<=80;  


 ### 6	查询scores表中成绩为85，86或88的记录
SELECT * FROM scores  
WHERE degree=85 OR degree=86 OR degree=88  

SELECT *  
FROM scores  
WHERE degree IN (85,86,88);  


 ### 7	查询“95031”班的“男”同学的记录
SELECT *  
FROM students  
WHERE class='95031' AND ssex='男';  


 ### 8	查询“95031”班或性别为“女”的同学记录
SELECT *  
FROM students  
WHERE class='95031' OR ssex='女';  


 ### 9	查询teacher表所有的系即不重复的depart列
SELECT DISTINCT depart FROM teachers;  


 ### 10	以班级class降序查询students表的所有记录
SELECT *  
FROM students   
ORDER BY class DESC;  


 ### 10	以班级class降序查询students表的所有记录
SELECT *  
FROM students  
ORDER BY class DESC;  


 ### 11	以cno升序、degree降序查询scores表的所有记录
SELECT *  
FROM scores  
ORDER BY cno ASC, degree DESC;  


 ### 12	查询“95031”班的学生人数
SELECT COUNT(1) AS stunum  
FROM students  
WHERE class='95031';  


 ### 13	查询每个班的学生人数
SELECT class,COUNT(class) FROM students  
GROUP BY class;  


 ### 14	查询至少有2名女生的班号
SELECT class,COUNT(1)  
FROM students  
WHERE ssex='女'  
GROUP BY class  
HAVING COUNT(1)>=2;	#直接使用聚合函数  

SELECT class,COUNT(1) AS girlcount  
FROM students  
WHERE ssex='女'  
GROUP BY class  
HAVING girlcount>=2;	#使用别名  


 ### 15	查询学生中最高分和最低成绩
SELECT MAX(degree) FROM scores;  
SELECT MIN(degree) FROM scores;  
SELECT MAX(degree),MIN(degree) FROM scores;  


 ### 16	查询学生中最大和最小的出生年月
SELECT MIN(sbirthday) FROM students  
SELECT MAX(sbirthday) FROM students  
SELECT MIN(sbirthday),MAX(sbirthday) FROM students  


 ### 17	查询大于平均分的学生
SELECT *  
FROM scores  
WHERE degree > (SELECT AVG(degree) FROM scores)  

SELECT DISTINCT sno  
FROM scores  
WHERE degree > (SELECT AVG(degree) FROM scores)  


 ### 18	查询scores表中的最高分的学生学号和课程号
#通过嵌套子查询  
SELECT sno,cno FROM scores  
WHERE degree = (SELECT MAX(degree) FROM scores);  

#通过排序取第一条记录  
SELECT sno,cno FROM scores  
ORDER BY degree DESC   
LIMIT 1  


 ### 19	查询‘3-105’号课程的平均分
SELECT AVG(degree),ROUND(AVG(degree),2)   
FROM scores WHERE cno='3-105';  

SELECT AVG(degree),ROUND(AVG(degree),2) FROM scores  
GROUP BY cno  
HAVING cno='3-105'  


 ### 20	查询各科的平均分
SELECT cno,ROUND(AVG(degree),2)   
FROM scores   
GROUP BY cno;  


 ### 21	查询每个学生的多科总成绩
SELECT sno,SUM(degree) FROM scores;  


 ### 22	查询95033班和95031班全体学生的记录
SELECT * FROM students   
WHERE class=95033 OR class=95031;  

SELECT *  
FROM students  
WHERE class IN ('95033','95031')  
ORDER BY class;  

#union 把两个结果集相加  
select sno,sname as s1 from students where class='95033'  
union  
SELECT sno,sname as s2 FROM students WHERE class='95031'  


 ### 23	查询存在有85分以上成绩的课程cno
SELECT DISTINCT cno  
FROM scores  
WHERE degree>85;  


 ### 24	查询所有教师和同学的name、sex和birthday
SELECT tname AS NAME,tsex AS sex, tbirthday AS birthday   
FROM teachers  
UNION  
SELECT sname AS NAME,ssex AS sex, sbirthday AS birthday   
FROM students;  

SELECT tname,tsex,tbirthday  
FROM teachers  
UNION  
SELECT sname,ssex,sbirthday  
FROM students;  

#字段个数相同即可，甚至类型不同都可以，但是业务逻辑不正确  
SELECT tname,tsex,NOW()  
FROM teachers  
UNION  
SELECT sname,ssex,sname  
FROM students;  


 ### 25	查询所有“女”教师和“女”同学的name、sex和birthday
SELECT sname,ssex,sbirthday FROM students WHERE ssex='女'  
UNION  
SELECT tname,tsex,tbirthday FROM teachers WHERE tsex='女';  


 ### 26	查询所有任课教师的tname和depart
课程表courses中有老师编号tno的就算任课
SELECT tname,depart  
FROM teachers  
WHERE tno IN (  
    SELECT DISTINCT tno  
    FROM courses  
);  


 ### 27	查询所有未讲课的教师的tname和depart
SELECT tname,depart  
FROM teachers  
WHERE tno NOT IN(  
    SELECT tno  
    FROM courses  
);  


 ### 28	查询同名的同学记录
先插入条测试数据：  
INSERT INTO STUDENTS (SNO,SNAME,SSEX,SBIRTHDAY,CLASS) VALUES (110 ,'王芳' ,'女' ,'1978-02-10',95031);  
姓名相同：王芳，性别相同，但出生年月不同。一个75年，一个78年。  
  
SELECT * FROM students  
WHERE sname IN   
(  
	SELECT sname FROM students   
	GROUP BY sname   
	HAVING COUNT(sname)>1  
);  


 ### 29	最大年龄的学员
SELECT * FROM students   
ORDER BY sbirthday   
LIMIT 1  

SELECT YEAR(NOW())-YEAR(s.sbirthday) AS age,s.* FROM students  s  
ORDER BY sbirthday   
LIMIT 1  


 ### 30	每个班上最小年龄的学员  
年龄来说，最大（MIN）和最小（MAX）和SQL查询出来刚好相反  
SELECT class,MAX(sbirthday) FROM students  
GROUP BY class  


 ### 31	全部学生按出生年月排行
SELECT * FROM students  
ORDER BY sbirthday  
日期是按数字比较的，不是按我们生活中日期的顺序，恰好反过来的。如77和76，数字比77大于76的，生活中76年生的肯定大于77年生的。  


 ### 32	查询学生的姓名和年龄
SELECT sname,YEAR(NOW())-YEAR(sbirthday) AS sage  
FROM students;  


 ### 33	以班号和年龄从大到小的顺序查询student表中的全部记录 
SELECT *  
FROM students  
ORDER BY class DESC,sbirthday DESC;  


 ### 34	查询男教师及其所上的课程
可以直接用表名.字段名访问，两个表都有tno字段，就必须指明是哪个表的tno，唯一名称就无需指定，如tname字段
SELECT teachers.tno,tname,cname FROM teachers   
INNER JOIN  
courses ON teachers.tno = courses.tno AND teachers.tsex='男'  

#表缩写 t
SELECT t.tname, c.cname  
FROM teachers t  
INNER JOIN courses c ON t.tno = c.tno AND t.tsex='男';  


 ### 35	查询各门课程的最高分同学的sno、cno和degree列
SELECT sno,cno,degree FROM scores  
WHERE degree IN (  
	SELECT MAX(degree) FROM scores GROUP BY cno  
)  

SELECT   
  s.sno,g.cno,s.degree  
FROM  
(SELECT cno,MAX(degree) maxd FROM scores GROUP BY cno) g  
LEFT JOIN  
(SELECT sno,cno,degree FROM scores) s  
ON g.cno=s.cno AND g.maxd=s.degree;  
可以看出使用两种方式都能满足查询，那该使用哪个呢？一般认为数据少时使用in，数据多时使用join。  


 ### 36	查询课程对应的老师姓名、职称、所属系
SELECT c.cname,t.tname,t.prof,t.depart  
FROM teachers t  
LEFT JOIN courses c ON t.tno = c.tno;  
  
SELECT   
	c.cno,c.cname,  
	t.tno,t.tname,t.prof,t.depart  
FROM  
	(SELECT * FROM teachers) t  
LEFT JOIN  
	(SELECT * FROM courses) c  
ON c.tno=t.tno;  
上面两种方式实现了，哪种方式更有呢？第一种方式简洁，第二种方式高效。简洁一目了然，高效怎么体现呢？下面的案例就得到了很好的证明。  


 ### 37	查询课程对应的女老师姓名、职称、所属系
SELECT c.cname,t.tname,t.prof,t.depart  
FROM teachers t  
LEFT JOIN courses c ON t.tno = c.tno   
WHERE t.tsex='女'  

SELECT   
	c.cno,c.cname,  
	t.tno,t.tname,t.prof,t.depart  
FROM  
	(SELECT * FROM teachers where tsex='女') t  
LEFT JOIN  
	(SELECT * FROM courses) c  
ON c.tno=t.tno;  
 
第一种先连接数据后过滤数据，假如数据量很大，第一种中间过程要构建巨大的临时表。而第二种方式先过滤数据，构建的中间结果集自然就变的很小。所占内存，所加工的时间所网络传输的时间都变少了，所以效率高。  


 ### 38	课程号“3-105”的前3名学员排行  
SELECT sno,degree FROM scores s   
WHERE cno='3-105'  
ORDER BY degree DESC  
LIMIT 3  


 ### 39	课程“计算机导论”的前3名学员排行  
SELECT sno,degree FROM scores s   
WHERE cno= (SELECT cno FROM courses WHERE cname='计算机导论')  
ORDER BY degree DESC  
LIMIT 3  


 ### 40	课程号“3-105”的倒数最后3名学员排行
先升序，就是低成绩在前，高成绩在后，然后取前3，但成绩是正序啊，形成嵌套查询，再倒序即可  
SELECT * FROM  
(  
	# 倒数的3个学员，但成绩是升序  
	SELECT sno,degree FROM scores s   
	WHERE cno='3-105'  
	ORDER BY degree  
	LIMIT 3  
) s  
ORDER BY degree DESC  #将成绩倒序  