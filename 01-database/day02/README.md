# day02
## 知识回顾
### 数据库高级
### 事务 transaction
如果执行SQL时有多步操作（多句SQL语句、不是查询新增、修改、删除）
这时为了多个用户同时操作互相影响，必须使用事务。
### 在mysql中如何去实现事务呢？
两种方式：  
- 1）事务相关命令 begin事务开启，commit事务提交，rollback事务回滚  
- 2）保存点 savepoint，savepoint chen设置保存点，rollback to chen 回滚到保存点时状态  
保存点额外操作，它会占用额外空间，什么时候释放呢？  
保存点处理完，最终提交提交commit，提交后保存临时数据就被删除了  

事务概念：ACID
A 原子性 C 一致性 I 隔离性 D 持久性
有4个隔离级别：可读取未提交，读取已提交，可重复读，串行化（加锁）
隔离级别越低，性能越高，安全性差；隔离级别越高，性能越低，安全性高。
实际开发中不可能鱼和熊掌兼得，做不到，取平衡点。  
oracle隔离读已提交，mysql隔离可重复读。

扩展：spring框架，它程序中使用事务；rabbitmq,rocketmq,kafka消息队列，分布式环境分布式事务

## 子查询：
实现更复杂SQL语句，更灵活，在select语句中在嵌套select语句
- 单值单列 = 
- in子查询串 多值单列 in (集合)

## 表关联
指数据库表之间关系，有4种关系：
一对一 one to one（1:1）：在子表中主外键是一个值，它们存储的都是主表中的主键值
一对多 one to many（1:n）：在子表中增加一列，存储的是主表中对应值 deptno=2
多对一 many to one（n:1）：反过来就是一对多
多对多 many to many（n:n）：需求额外加一张表来存储这两个表的关系，中间表

## 索引 index
数据库索引提高查询性能，CRUD操作，新增，修改，删除，查询，SQL好入门，难精通
索引单独创建，它是表的副本（复制版本，克隆版本）部分。  
排序，对索引列进行排序，可以引入二分查找快速查找方法，检索速度明显提高，达到百倍以上  
全表遍历（最慢方式），全索引遍历（不排序），二分查找（排序）  
数据量小看不出来，也不用创建索引，但是如果数据量很大（万条记录以上，效果就非常明显）

查询数据，就变成先查询索引表，快速定位后，找到对应数据。

索引分类：  
1）单值索引：主键索引（自动，where id=100）, unique 唯一索引，数据库可以进行数据唯一值检查  
2）复合索引：多个索引列（phone+cardno）  

什么情况下，什么列来创建索引呢？  
常在where条件或者orderby排序频繁出现的列，就单独给它们创建索引  
where phone like '135%' and cardno like '610113%'  

# 今日任务
表设计（系统分析师）、面试题
初级程序员、中级程序员、高级程序员（系统分析师-怎么把用户需求转变设计，表，怎么构成，类，类的调用）
对于初级程序员要求，能看到表设计的图，来利用设计文档生成创建数据库表的SQL语句，在数据库中创建表

## 表设计
表设计使用什么工具呢？最成熟的PowerDesigner 15
PD创建模型model，提供了很多模型，物理数据模型pdm
创建物理数据模型，还要选择数据库DBMS，它可以生成对应数据库的创建表的sql语句

### 表设计的过程：
1）创建表，设置表名  
2）创建表的字段，设置字段名称（英文），展示名称（中文），字段类型，字段长度，主键，外键  
3）关系  
4）生成创建表的sql语句，直接复制到sqlYog执行即可    

在实际开发中，不创建外键关联，这样表更加灵活  

![输入图片说明](https://images.gitee.com/uploads/images/2021/0129/134816_f4cbd9e3_2325268.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0129/135319_7c280f69_2325268.png "屏幕截图.png")


### 常见的3个问题：
1） P 主键（这个字段的值就可以代表这条记录）、F 外键（某个表的主键在其他从表中）、M（非空，默认null，插入记录时，就必须填写，不填写数据库报错）  
2） numeric （mysql：decimal）浮点 (8,2) 整数位几位？（6位整数）小数位数是几位？(2位小数，会四舍五入）  
3） 创建表时，实际开发中习惯不创建外键约束（表之间关系，保证数据正确性，但是测试时不方便，太死板
 测试时不加外键约束，靠开发者自身去填写正确数据；程序上线正式运行时，在加外键约束）  

### 数据库mysql和java中字段类型：
String          varchar
int             int
long            long
float/double    numerice/decimal
date            datatime/timestamp 时间戳

### 外键的sql语句（了解，外键创建使用不方便，不适合分布式环境）
>  alter table item add constraint FK_Reference_4 foreign key (cid)
>        references item_cat (cid) on delete restrict on update restrict;

alert table 修改表结构    
item 表名  
add constraint 增加约束（外键约束）  
FK_Reference_4 外键约束名字，pd自动产生，FK外键，Reference引用，4第四个  
foreign key FK 外键关键字  
（cid）外键名称，和主表中名称一致（习惯）  

习惯不创建外键，而靠自己写代码保证数据的正确性！！！

## 面试题
40道题，覆盖这几天所学这些知识点，分散，每个题独立，它们没有关系。
  references 哪个表来调用 item_cat (cid) 主表
什么时候数据库会进行检查呢？on delete restrict on update restrict 数据进行删除和修改时会进行检查
