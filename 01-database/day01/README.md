# day01
## 事务 transaction
### 概念
4个特征：ACID  
- A 原子性：它是不可分隔，在做数据库操作时，如果有多个动作（转账，A转给B=100w，A的账户-100w，B的账户+100w）
     如果成功，提交事务；如果失败，回滚事务。不能有中间状态。    
- C 一致性：数据一致，总额一定，数据保证不多不少。  
- I 隔离性：并发时，不能互相干扰，（java多线程是如何做的呢？锁），4个隔离性从松散到紧密，  
    隔离级别越小，速度越快，安全性越差；隔离级别越大，速度越慢，安全性越好。  
    4个隔离级别：读未提交、读已提交、可重复读、串行化（加锁）  
- D 持久性：内存和磁盘，如果突然断电。数据库保证数据一旦写入不会丢失。  

mysql、oracle称为关系型数据库，是支持事务的；
但是并不是所有数据库都支持，redis,hbase不完全支持事务

mysql提供事务支持，它就提供一系列的SQL命令：
mysql中默认事务直接提交（自动提交），需要改成手动提交

>  set @@autocommit = 0;         //默认是1自动提交，0手动提交  
>  begin;                        //开启事务  
>  commit;                       //提交事务  
>  rollback;                     //回滚事务（就当什么事情也没有发生，数据保持一致）  

推荐大家使用sqlYog，它的操作和navcat操作方式有不同，事务操作时，两个工具展现结果不同  
下载链接：http://act.codeboy.com/tools/00/

## 准备测试环境
1）创建一个mysql-db数据库，设置中文字符集utf8，否则容易乱码
2）创建数据库表 emp(id,salary）
3）利用事务来模拟转账

### 模拟转账，100用户500万，200用户0万

> 100转账给200,100万；  
> 两个SQL，100：salary=salary-100；200：salary=salary+100;  
> update emp set salary=salary-100 where id=100;  
> update emp set salary=salary+100 where id=200;  

SET @@autocommit = 0; #设置事务手动提交  
BEGIN; #只有多步操作时，才需要事务  

UPDATE emp SET salary=salary-100 WHERE id=100;  
UPDATE emp SET salary=salary+100 WHERE id=200;  

COMMIT; #手动提交，当所有操作都成功时  
ROLLBACK; #回滚，当某一步操作失败，就必须回滚  
#数据库事务保障数据安全，数据的一致性，事务存在的意义  

模拟多用户操作，演示隔离级别：
两种情况：  
1）一个A用户开了两个的查询窗口   
2）另外打开一个sqlYog，相当于另外一个B用户  
当操作过程中，没有提交事务之前：   
A用户的第二个查询窗口能看到第一个窗口执行结果吗？  可以，同一个用户事务是可见的  
B用户能看到A用户的操作吗？不可以，当A事务操作没有完成时，B用户是无法看到，这就是数据库事务隔离级别实现  

### 删库跑路（删除全表数据，删错了，怎么办？怎么预防它呢？）存储点 savepoint

> 开启事务 begin;
> 设置一个存储点（保存点）savepoint chen;（存储点名称，自定义）
> 删除操作 delete from emp;
> 如果删除正确，提交事务 commit;
> 如果误删除了，回滚到保存点 rollback to chen;（这个存储点名称和上面一致）


## 子查询
### 单值子查询：
> SELECT * FROM emp   
> WHERE job = (SELECT job FROM emp WHERE empno=200)  
子查询返回结果必须是单个字段，必须是单个值，才能用=

### 多值子查询：  
> SELECT * FROM emp   
> WHERE job IN (SELECT job FROM emp WHERE empno IN (100,200) )  
in子查询串，返回结果可以是多个值，但是必须是单列

## *表关联
数据库表table，表之间有关系吗？
dept 部门表/emp员工表/empext员工扩展信息表/student学生表
部门和员工关系：有关系，一个部门下有多个员工
部门和员工扩展信息表：没有直接关系
部门和学生：没有关系
员工和员工扩展信息表：有关系，一个员工可以有一个扩展信息

有什么关系？
有四种关系：
- 一对一：员工表和员工的扩展信息表一对一  
- *一对多：部门表和员工表（一个部门有多个员工），商品分类和商品（一个商品分类下可以有多个商品）  
- 多对一：反过来就是一对多，谁在前面。员工表和部门表？（多个员工从属一个部门）  
- 多对多：老师和学生（一个老师教多个学生，一个学生可以听多个老师课）  

小结4种关系在数据库表中如何设计？  
- 1:1，主表（前面的叫主表）的主键就是子表（后面的叫从表）的主键PK emp.empno=empext.empno  
- 1:n，主（dept）从（emp）关系，在子表中添加记录时增加一个外键FK  
- n:1，反过来  
- n:n，需要一张中间表来存储两边的主键，它们两个和起来叫做复合主键（多个字段构成）  

![输入图片说明](https://images.gitee.com/uploads/images/2021/0128/144236_4327264a_2325268.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0128/151533_5e0e4fc4_2325268.png "屏幕截图.png")

### 列出所有部门下的所有的员工

SELECT * FROM dept
SELECT * FROM emp

SELECT * FROM dept,emp 3*5 笛卡尔积，直积（数学概念）

### 找到部门名称和员工名称  
SELECT dept.deptno,dept.dname,emp.ename FROM dept,emp  
WHERE dept.deptno = emp.deptno  


#简写
SELECT d.dname,e.ename FROM dept d,emp e  
WHERE d.deptno = e.deptno

## 表关联：
INNER JOIN 内连接、LEFT JOIN 左连接、RIGHT JOIN 右连接  

### 列出所有部门所有员工信息  
SELECT d.dname,e.ename   
FROM dept d INNER JOIN emp e  
ON d.deptno = e.deptno  

### 列出所有员工信息和扩展信息  
SELECT e.ename,e.job,  
 t.address,t.phone,t.cardno  
FROM emp e INNER JOIN empext t  
ON e.empno = t.empno  

### ?其他员工去哪了？
SELECT e.ename,e.job,  
 t.address,t.phone,t.cardno  
FROM emp e RIGHT JOIN empext t  
ON e.empno = t.empno  

### 三种方式的区别：

INNER JOIN两边都对应有记录的才展示，其他去掉  
LEFT JOIN左边表中的数据都出现，右边没有数据以NULL填充  
RIGHT JOIN右边表中的数据都出现，左边没有数据以NULL填充  

![输入图片说明](https://images.gitee.com/uploads/images/2021/0128/160800_52704653_2325268.png "屏幕截图.png")


### 列出research部门下的所有员工的信息
SELECT deptno FROM dept WHERE dname='research'  

SELECT * FROM emp  
WHERE deptno=  
(SELECT deptno FROM dept WHERE dname='research')  

### ?怎么用左链接 INNER JOIN 实现上面的需求？  
SELECT d.dname,e.ename,e.job  
FROM emp e INNER JOIN dept d  
ON e.deptno=d.deptno 
WHERE d.dname='research'  

### 列出tony3他的扩展信息  
SELECT *  
FROM emp e INNER JOIN empext t  
ON e.empno=t.empno 
WHERE e.ename='tony3'  


## *索引
### 什么是数据库表的索引？index 快速访问数据库表中记录作用
全表遍历，索引表，索引表遍历，排序，二分查找
找empno=500员工信息？

![输入图片说明](https://images.gitee.com/uploads/images/2021/0128/165152_e3a9f1bd_2325268.png "屏幕截图.png")

主键会自动创建索引，索引是要单独占用空间的，它创建时进行排序，排序的目的就为了可以快速检索
利用类似二分查找的方式快速的找到所要数据，比全表遍历，全索引遍历速度要高百倍以上，
数据量小不明显，数据量越大效果越好

### 索引数据库优化第一步。
优点：性能高  
缺点：额外占用空间，也称为冗余。新增，修改，删除都会造成索引需要重构（重新排序）排序需要额外时间  
在数据量小时推荐索引，如果数据量很大不推荐（大数据方式），索引不要滥用。  
一般在where条件中字段，在order by排序中的字段才需要创建索引。

### 索引查询过程：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0128/170209_fef461ee_2325268.png "屏幕截图.png")

### ？自己如何创建索引？
#电话号码唯一验证，unique index 唯一索引设定  
ALTER TABLE empext ADD UNIQUE (phone)  
ALTER TABLE empext ADD UNIQUE (cardno)  

### 删除索引
ALTER TABLE empext DROP INDEX cardno  

#### 复合索引，多个索引列，提高查询速度，不具备唯一性判断  
ALTER TABLE empext   
ADD INDEX idx_phone_cardno(phone,cardno)  
