# day04
## 知识回顾
### JDBC
java如何去访问数据库，java提供一套访问数据库接口规范java.sql.*，数据库厂商（oracle/mysql等等）
根据接口规范实现java来访问某个厂商数据库。
把程序和数据库进行连接。executeQuery执行sql语句。

### 提供3个途径
1）statement 语句对象，执行普通sql，没有预编译，每次提交sql，mysql进行编译处理
2）preparedStatement，预编译语句对象，提前把主干sql部分进行编译，进行缓存，下次调用无需再次编译。
    select * from tearcher where tname=? ，前面部分无需再次编译，后面的参数只这部分进行编译和主干连接  
    这种方式比statement性能高。  
    防止黑客侵入（SQL注入），利用sql'单撇拼接作用形成特殊结构，造成sql误解析   
    默认数据库SQL tname='陈冰'，陈冰是用户输入的内容，用户非法输入：tname='陈冰' or 1=1 or ''    
    ？问号占位符形式，陈冰' or 1=1 or '这个用户输入进行转义：陈冰\' or 1=1 or \'，把单撇就变成字符，不是sql语句字符串定界符  
    这样就解决了SQL注入问题。所以主流开发推荐使用preparedStatement，mybaits持久层框架底层jdbc,preparedStatement实现
3）callStatement 存储过程，这项技术废除。  
    a.存储过程是数据库端的编程，每个数据库厂商的语法不兼容。如果换数据库（迁移）这些存储过程都要重新，它比java难10倍  
    b.不适合分布式环境，它被淘汰。阿里手册就禁止使用存储过程。

## GIT

### 版本控制
保留多个版本，形成多个副本历史记录。这样防止代码丢失。在软件开发团队中进行协作。代码共享，代码冲突（多人编辑同一个文件）
主流：CVS（没人使用）、SVN、GIT  

### GIT它能取代SVN吗？  
取代不了，SVN管理代码和文档，GIT管理代码；SVN权限粒度更细（每个文件去设置权限），  
GIT权限粒度粗（对整个仓库）

### 网盘和GIT区别？
网盘管理文件，文件上传和文件下载，没有冲突，要么覆盖，要么改名，分享。秒传。 
GIT 代码管理，有一整套git命令，团队协作，代码文件提交，推送，拉取。多个历史备份（版本控制）。

## 网站开发

授课计划：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0202/111249_ade3b8f6_2325268.png "屏幕截图.png")


### 创建git仓库和本地仓库关联
mkdir web2012	创建目录
cd web2012	进入目录				休息：15分钟，11:08继续
						下面要开始使用HBuilderX了，大家提前打开

（执行一次）
git init			init初始化，一套系统，执行后会产生隐藏目录.git，这个目录就标识为git受控
touch README.md		创建README.md文件（默认空）

（反复执行）
git add README.md		把README.md文件提交本地索引（暂存区）
（反复执行）
git commit -m "first commit"	提交本地索引到本地仓库，-m参数，备注信息，为什么提交
			
			把远程仓库和本地这个目录建立联系
			当前目录web2012，https://gitee.com/nutony/web2012.git
			origin 原始库（分支）
（执行一次）
git remote add origin https://gitee.com/nutony/web2012.git

（反复执行）	
git push -u origin master	推送，把本地仓库代码提交到远程仓库
			-u参数，origin原始，master主分支
git push
	如果过程中提示输入用户名和密码，输入你自己gitee 的用户名和密码
	https://gitee.com/nutony/web2012.git

### 系统架构图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0202/113347_68a1ece8_2325268.png "屏幕截图.png")

## 使用HBuilderX
### 前端的开发工具

![输入图片说明](https://images.gitee.com/uploads/images/2021/0202/145225_897d6615_2325268.png "屏幕截图.png")

